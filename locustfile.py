import json
import random
import uuid
from locust import HttpUser, task, between

USERGUID = '45bbdb26-3d7b-4648-acac-80f2208e9f62'
SESSIONTOKEN = '7eed3d2a-ff0c-4716-b50a-af38a10f7d2a'
HEADERS = {"Content-Type": "application/json", "x-capi-userguid": USERGUID, "x-capi-sessiontoken": SESSIONTOKEN}
HOST = 'https://api.sonicdrivein-nonprod.digital/poc-test-api-preprod'
PROFILES = './resources/preprod-profiles.csv'
APPLE_PROFILE_IDS = './resources/appleprofileids'
FACEBOOK_IDS = './resources/facebookids'


class SimulatedUser(HttpUser):
    host = HOST
    wait_time = between(0.5, 3)
    user_guid = None
    user_email = None
    facebook_id = None
    apple_id = None

    @task(76)
    def get_identities_with_userguid(self):
        with open(PROFILES) as f:
            line = f.read().splitlines()
            self.user_guid = random.choice(line).split(',').pop(1)
        params = {"UserGUID": self.user_guid}
        resp = self.client.get("/customer/manage/identities?", params=params, headers=HEADERS,
                               name="Get identities with userGUID")
        if resp.status_code != 200:
            print(resp.status_code)
            print(resp.content)
        assert resp.status_code == 200

    @task(6)
    def get_identities_with_email(self):
        with open(PROFILES) as f:
            line = f.read().splitlines()
            self.user_email = random.choice(line).split(',').pop(2)
        params = {"emailAddress": self.user_email}
        resp = self.client.get("/customer/manage/identities", headers=HEADERS, params=params,
                               name="Get identities with email address")
        if resp.status_code != 200:
            print(resp.status_code)
            print(resp.content)
        assert resp.status_code == 200

    @task(3)
    def get_identities_with_facebook(self):
        with open(FACEBOOK_IDS) as f:
            self.facebook_id = random.choice(f.read().splitlines())
        params = {"FacebookID": self.facebook_id}
        resp = self.client.get("/customer/manage/identities", headers=HEADERS, params=params,
                               name="Get identities with FacebookID")
        if resp.status_code != 200:
            print(resp.status_code)
            print(resp.content)
        assert resp.status_code == 200

    @task(3)
    def get_identities_with_apple(self):
        with open(APPLE_PROFILE_IDS) as f:
            self.apple_id = random.choice(f.read().splitlines())
        params = {"AppleID": self.apple_id}
        resp = self.client.get("/customer/manage/identities", headers=HEADERS, params=params,
                               name="Get identities with AppleID")
        if resp.status_code != 200:
            print(resp.status_code)
            print(resp.content)
        assert resp.status_code == 200

    @task(6)
    def create_identities_with_email_address(self):
        random_email = str(uuid.uuid4()) + '@gmail.com'
        payload = json.dumps({"EmailAddress": random_email})
        resp = self.client.post("/customer/identities", headers=HEADERS, data=payload,
                                name="Create identities with email address")
        if resp.status_code != 201:
            print(resp.status_code)
            print(resp.content)
        assert resp.status_code == 201

    @task(3)
    def create_identities_with_facebook(self):
        random_uuid = str(uuid.uuid4())
        random_email = random_uuid + '@gmail.com'
        payload = json.dumps({"EmailAddress": random_email, "FacebookProfileID": random_uuid})
        resp = self.client.post("/customer/identities", headers=HEADERS, data=payload,
                                name="Create identities with Facebook")
        if resp.status_code != 201:
            print(resp.status_code)
            print(resp.content)
        assert resp.status_code == 201
        with open(FACEBOOK_IDS, 'a') as f:
            f.write(random_uuid + "\n")

    @task(3)
    def create_identities_with_apple(self):
        random_uuid = str(uuid.uuid4())
        random_email = random_uuid + '@gmail.com'
        payload = json.dumps({"EmailAddress": random_email, "AppleProfileID": random_uuid})
        resp = self.client.post("/customer/identities", headers=HEADERS, data=payload,
                                name="Create identities with Apple")
        if resp.status_code != 201:
            print(resp.status_code)
            print(resp.content)
        assert resp.status_code == 201
        with open(APPLE_PROFILE_IDS, 'a') as f:
            f.write(random_uuid + "\n")
