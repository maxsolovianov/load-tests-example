## How to run (mac os x):

1. brew install python3
2. pip3 install virtualenv
3. cd load-tests-sonic/
4. virtualenv -p python3 venv
5. source venv/bin/activate
6. pip install -r requirements.txt
7. locust
8. go to http://0.0.0.0:8089/